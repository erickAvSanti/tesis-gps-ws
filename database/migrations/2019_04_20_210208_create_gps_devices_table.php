<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpsDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gps_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand',100);
            $table->string('model',100);
            $table->string('imei',100);
            $table->string('simcard',100);
            $table->string('operator',100);
            $table->string('phone_number',100);
            $table->text('description');
            $table->unique(['brand','model','imei','phone_number'],'gps_unique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gps_devices', function (Blueprint $table) {
            $table->dropUnique('gps_unique');
        });
        Schema::dropIfExists('gps_devices');
    }
}
