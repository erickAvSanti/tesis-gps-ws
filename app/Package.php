<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

	protected $dates = ['delivered_at'];
    public function products(){
        return $this->belongsToMany(Product::class,'product_packages','packages_id','products_id')->withPivot('quantity');
    }
    public function client()
	{
	    return $this->belongsTo(Client::class,'clients_id','id');
	}
    public function gps_device()
	{
	    return $this->belongsTo(GpsDevice::class,'gps_devices_id','id');
	}
    public function user()
	{
	    return $this->belongsTo(User::class,'users_id','id');
	}
    public function get_products()
	{
	    return $this->products()->where('packages_id',$this->id);
	}
}
