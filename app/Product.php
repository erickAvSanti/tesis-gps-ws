<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public function packages(){
        return $this->belongsToMany(Package::class,'product_packages','packages_id','products_id')->withPivot('quantity');
    }
    public static function search($str)
    {
        return Product::where(function($query) use($str){
            $query->where('name','LIKE',"%$str%")
            ->orWhere('description','LIKE',"%$str%")
            ->orWhere('sku','LIKE',"%$str%");
        })->get();
    }
    public static function guid()
	{
	    if (function_exists('com_create_guid') === true)
	    {
	        return trim(com_create_guid(), '{}');
	    }

	    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}
}
