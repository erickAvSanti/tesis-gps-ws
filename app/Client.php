<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Package;
class Client extends Model
{
    public function packages(){
        return $this->hasMany(Package::class,'clients_id','id');
    }
}
