<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Client;

class ClientsController extends Controller
{
    public function list()
    {
        $data = Client::all();
        return response()->json($data);
    }
    private static function &fillData(&$request,&$model){ 
        $request->merge(array_map('trim', $request->all()));
        $model->firstname = $request->firstname;
        $model->lastname = $request->lastname;
        $model->address = $request->address ? $request->address : '';
        $model->document = $request->document && strlen($request->document)>0 ? $request->document : null;
        $model->email = $request->email && strlen($request->email)>0 ? $request->email : null;
        $model->phone_number = $request->phone_number && strlen($request->phone_number)>0 ? $request->phone_number : null;
        return $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function getPropsValidation($request){

        $props = [];

        if($request->document && strlen($request->document)>0){
            $props['document'] = 'min:7';
        }
        if($request->firstname && strlen($request->firstname)>0){
            $props['firstname'] = 'required|min:2';
        }
        if($request->lastname && strlen($request->lastname)>0){
            $props['lastname'] = 'required|min:2';
        }
        if($request->email && strlen($request->email)>0){
            $props['email'] = 'email|unique:clients';
        }
        if($request->phone_number && strlen($request->phone_number)>0){
            $props['phone_number'] = 'min:7';
        }
        if($request->address && strlen($request->address)>0){
            $props['address'] = 'required|min:3';
        }
        return $props;
    }
    public function store(Request $request)
    {
        $props = $this->getPropsValidation($request);
        $this->validate($request, $props);
        $record = new Client;
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $props = $this->getPropsValidation($request);
        $this->validate($request, $props);
        $record = Client::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Client::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
