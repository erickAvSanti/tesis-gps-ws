<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\GpsDevice;
use Illuminate\Support\Facades\Storage;

class GpsDeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $data = GpsDevice::all();
        return response()->json($data);
    }
    public function store(Request $request)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = new GpsDevice;
        if($record){
            $record = &self::fillData($request,$record,true);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private static function &fillData(&$request,&$model){ 
        $model->brand = $request->brand ? $request->brand : '';
        $model->model = $request->model ? $request->model : '';
        $model->imei = $request->imei ? $request->imei : '';
        $model->simcard = $request->simcard ? $request->simcard : '';
        $model->operator = $request->operator ? $request->operator : '';
        $model->phone_number = $request->phone_number ? $request->phone_number : '';
        $model->description = $request->description ? $request->description : '';
        /*
        if($add_sku){
            $model->sku = Product::guid();
        } 
        */
        return $model;
    }
    public function update(Request $request, $id)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = GpsDevice::find($id);
        if($record){
            $record = &self::fillData($request,$record,false);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = GpsDevice::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
