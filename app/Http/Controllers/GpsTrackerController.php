<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\GpsTrack;
use \App\Package;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class GpsTrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $date = $request->date;
        $imei = $request->imei;
        $users_id = $request->users_id;
        $delivered = $request->delivered;
        $imeis = array();
        $result = [
            "result"=>false,
            "date"=>$date,
            "imei"=>$imei,
            "users_id"=>$users_id,
            "delivered"=>$delivered,
        ];
        if(isset($date) and preg_match("/\d{4}-\d{1,2}-\d{1,2}/", $date)===1){
            $d1 = Carbon::parse($date);
            $d2 = $d1->clone()->addDays(1);


            $datetime1 = new \DateTime($d1->format('Y-m-d H:i:s'),new \DateTimeZone(config('app.timezone')));
            $datetime2 = new \DateTime($d2->format('Y-m-d H:i:s'),new \DateTimeZone(config('app.timezone')));
            $datetime1->setTimezone(new \DateTimeZone('GMT'));
            $datetime2->setTimezone(new \DateTimeZone('GMT'));

            /*
            $iso_d1 = $d1->toISOString();
            $iso_d2 = $d2->toISOString();
            $iso_d1 = preg_replace("/T(05:00:00)/","T00:00:00",$iso_d1);
            $iso_d2 = preg_replace("/T(05:00:00)/","T00:00:00",$iso_d2);
            */
            $iso_d1 = $datetime1->format(\DateTime::ATOM);
            $iso_d2 = $datetime2->format(\DateTime::ATOM);
            $utc_d1 = new \MongoDB\BSON\UTCDateTime(new \DateTimeImmutable($iso_d1));
            $utc_d2 = new \MongoDB\BSON\UTCDateTime(new \DateTimeImmutable($iso_d2));
            /*
            \Log::info("iso_d1 => ".$iso_d1);
            \Log::info("iso_d2 => ".$iso_d2);
            */
            /*
            \Log::info("utc_d1 => ".$utc_d1);
            \Log::info("utc_d2 => ".$utc_d2);
            \Log::info("d1 => ".$d1->toISOString());
            \Log::info("d2 => ".$d2->toISOString());
            */
            /*
            $builder = GpsTrack::whereBetween('timestamp',[
                new \DateTime($d1->toISOString()),
                new \DateTime($d2->toISOString())]
            );
            */
            

            if(is_numeric($users_id)){
                $packages = 
                    Package::where("users_id",$users_id)
                    ->where("delivery_day",$date);
            }else{
                $packages = 
                    Package::where("delivery_day",$date);
            }
            if(isset($delivered)){
                $packages->where("delivered",$delivered);
            }

            $packages = $packages->get();
            \Log::info($packages);

            $imeis_by_user = array();

            if(isset($packages)){
                foreach ($packages as $kpackage => &$vpackage) {
                    $gps_device = $vpackage->gps_device;
                    if($gps_device){
                        $imeis[] = $gps_device->imei;
                        if(!array_key_exists($vpackage->users_id,$imeis_by_user)){
                            $imeis_by_user[$vpackage->users_id] = array();
                        }
                        $imeis_by_user[$vpackage->users_id][] = $gps_device->imei;
                    }
                }
            }
            $data = array();
            $last_imei_positions = array();
            $users_alert = array();
            foreach ($imeis as $kimei => &$vimei) {
                $data_imei = GpsTrack::whereRaw([
                'timestamp' => 
                    array(
                        '$gte' => $utc_d1, 
                        '$lt' => $utc_d2
                    )
                ])->where('imei',$vimei)
                ->orderBy('timestamp','ASC')
                ->limit(50)
                ->project([
                    'lat'=>1,
                    'ln'=>1,
                    'timestamp'=>1,
                    '_id'=>1,
                ])
                ->get();

                $last_imei_positions[$vimei] = GpsTrack::whereRaw([
                'timestamp' => 
                    array(
                        '$gte' => $utc_d1, 
                        '$lt' => $utc_d2
                    )
                ])->where('imei',$vimei)
                ->orderBy('timestamp','DESC')
                ->project([
                    'lat'=>1,
                    'ln'=>1,
                    'timestamp'=>1,
                    '_id'=>1,
                ])
                ->first();


                foreach ($data_imei as $k_dimei => &$v_dimei) {
                    if($k_dimei>0){
                        $prev_v_dimei = $data_imei[$k_dimei-1];
                    }else{
                        $prev_v_dimei = null;
                    }
                    if($prev_v_dimei){
                        $lat1   = $v_dimei->lat;
                        $ln1    = $v_dimei->ln;
                        $lat2   = $prev_v_dimei->lat;
                        $ln2    = $prev_v_dimei->ln;
                        if(
                            is_numeric($lat1) && 
                            is_numeric($ln1) && 
                            is_numeric($lat2) && 
                            is_numeric($ln2)
                        ){
                            $haversine = GpsTrackerController::haversineGreatCircleDistance(
                                (float)$lat1,
                                (float)$ln1,
                                (float)$lat2,
                                (float)$ln2
                            );
                            $vincenty = GpsTrackerController::vincentyGreatCircleDistance(
                                (float)$lat1,
                                (float)$ln1,
                                (float)$lat2,
                                (float)$ln2
                            );
                            $v_dimei->haversine = $haversine;
                            $v_dimei->vincenty = $vincenty;
                        }else{
                            $v_dimei->diff = 0;
                        }
                    }else{
                        $v_dimei->diff = 0;
                    }
                }

                $data[$vimei] = $data_imei;

            }

            foreach ($imeis_by_user as $user_id => &$user_imeis) {
                if(
                    is_array($user_imeis) && 
                    count($user_imeis)>0
                ){
                    $user_valid_imeis = array();
                    foreach ($user_imeis as $kk => &$vv) {
                        if(
                            array_key_exists($vv,$last_imei_positions) && 
                            $last_imei_positions[$vv]!=null
                        ){
                            $user_valid_imeis[] = $last_imei_positions[$vv];
                        }
                    }
                    \Log::info($user_valid_imeis);
                    foreach ($user_valid_imeis as $ki => &$vi) {
                        $break = false;
                        foreach ($user_valid_imeis as $ki2 => &$vi2) {
                            if($vi->_id!=$vi2->_id){
                                $vincenty = GpsTrackerController::vincentyGreatCircleDistance(
                                    (float)$vi->lat,
                                    (float)$vi->ln,
                                    (float)$vi2->lat,
                                    (float)$vi2->ln
                                );
                                if($vincenty>10){
                                    $users_alert[] = $user_id;
                                    $break = true;
                                    break;
                                }
                            }
                        }
                        if($break)break;
                    }
                }
            }

            $result["imeis"]=$imeis;
            $content = $data;
            $result["data"]=$content;
            $result["imeis_by_user"]=$imeis_by_user;
            $result["last_imei_positions"]=$last_imei_positions;
            $result["users_alert"]=$users_alert;
            $result["result"]=true;

        }
        return response()->json($result);
    }
    private static function haversineGreatCircleDistance(
        $latitudeFrom, 
        $longitudeFrom, 
        $latitudeTo, 
        $longitudeTo, 
        $earthRadius = 6371000
    )
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $latDelta = $latTo - $latFrom;
      $lonDelta = $lonTo - $lonFrom;

      $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
      return $angle * $earthRadius;
    }
    private static function vincentyGreatCircleDistance(
        $latitudeFrom, 
        $longitudeFrom, 
        $latitudeTo, 
        $longitudeTo, 
        $earthRadius = 6371000
    )
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

      $angle = atan2(sqrt($a), $b);
      return $angle * $earthRadius;
    }
}
