<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Package;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $data = Package::all();
        foreach ($data as $key => &$value) {
            $value->client;
            $value->gps_device;
            $value->user;
            $value->products;
        }
        return response()->json($data);
    }
    public function delivery_now(){
        $cc = Carbon::now()->format('Y-m-d');
        $data = Package::whereDate('delivery_day',$cc)->get();
        foreach ($data as $key => &$value) {
            $value->gps_device;
        }
        return response()->json($data);
    }
    public function store(Request $request)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = new Package;
        if($record){
            $record = &self::fillData($request,$record,true);
            if($record->save()){
                $products = $record->products;
                $ids = $this->getProductsIDs($request);
                foreach ($ids as $key => $value) {
                    $products->attach($value['id'],['quantity'=>$value['quantity']]);
                }
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private static function &fillData(&$request,&$model,$add_sku=false){ 
        $model->name = $request->name ? $request->name : '';
        $model->description = $request->description ? $request->description : '';
        $model->clients_id = $request->clients_id && is_numeric($request->clients_id) ? $request->clients_id : NULL;
        $model->gps_devices_id = $request->gps_devices_id && is_numeric($request->gps_devices_id) ? $request->gps_devices_id : NULL;
        
        if($request->delivered!=$model->delivered && $request->delivered=="SI"){
            $model->delivered_at = Carbon::now();
        }

        if($request->delivered && ($request->delivered=="SI" || $request->delivered=="NO")){
            $model->delivered = $request->delivered;
        }
        $model->users_id = $request->users_id && is_numeric($request->users_id) ? $request->users_id : null;
        if($request->delivery_day){
            $model->delivery_day = $request->delivery_day;
        }
        /*
        if($add_sku){
            $model->sku = Package::guid();
        } 
        */
        return $model;
    }
    private function getProductsIDs($request){
        $pids = $request->products_id;
        $ids = array();
        if($pids){
            $ids = json_decode($pids,true);
            if(!is_array($ids))$ids = array();
        }
        return $ids;
    }
    public function update(Request $request, $id)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = Package::find($id);
        if($record){
            $record = &self::fillData($request,$record,false);
            if($record->save()){
                $products = $record->products();
                $products->detach();
                $ids = $this->getProductsIDs($request);
                foreach ($ids as $key => $value) {
                    $products->attach($value['id'],['quantity'=>$value['quantity']]);
                }
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Package::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function get_products($id){
        $package = Package::find($id);
        if($package){
            $package->get_products;
            return response()->json($package,200);
        }else{
            return response()->json(["msg"=>"package-not-found","id"=>$id],404);
        }
    }
}
