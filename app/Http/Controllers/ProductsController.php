<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $data = Product::all();
        return response()->json($data);
    }
    public function store(Request $request)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = new Product;
        if($record){
            $record = &self::fillData($request,$record,true);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private static function &fillData(&$request,&$model,$add_sku=false){ 
        $model->name = $request->name ? $request->name : '';
        $model->description = $request->description ? $request->description : '';
        if($request->sku && $request->sku!=''){
            $model->sku = $request->sku;
        }else{
            $model->sku = mt_rand();
        }
        /*
        if($add_sku){
            $model->sku = Product::guid();
        } 
        */
        return $model;
    }
    public function update(Request $request, $id)
    { 
        $request->merge(array_map('trim', $request->all()));
        $record = Product::find($id);
        if($record){
            $record = &self::fillData($request,$record,false);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
