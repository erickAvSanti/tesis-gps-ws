<?php 
namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GpsTrack extends Eloquent {

    protected $collection = 'tracking';
    protected $connection = 'mongodb';
    protected $dates = [
    	'timestamp',
    ];
    protected $casts = [
	];

}