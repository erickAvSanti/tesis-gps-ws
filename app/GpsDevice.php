<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsDevice extends Model
{
    public function packages(){
        return $this->hasMany(Package::class,'gps_devices_id','id');
    }
}
