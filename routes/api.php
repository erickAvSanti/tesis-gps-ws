<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::group(['middleware' => ['cors']], function () {

	Route::prefix('android/users')->group(function () { 

		Route::post('login', 'PassportController@login');
		Route::post('register', 'PassportController@register'); 

		Route::group(['middleware' => ['auth:api']], function () {
			$regex_id_int = '^[\d]+$';
		    Route::get('profile', 'PassportController@details'); 
		    Route::get('/', 'PassportController@list'); 
		    Route::put('/{id}', 'PassportController@update')->where(['id'=>$regex_id_int]);
		    Route::post('/', 'PassportController@register'); 
		    Route::delete('/{id}', 'PassportController@destroy')->where(['id'=>$regex_id_int]); 
		    Route::post('/logout', 'PassportController@logout'); 
		}); 
	
	});

	Route::prefix('android')->group(function () {  
		Route::group(['middleware' => ['auth:api']], function () { 
			$regex_id_int = '^[\d]+$';
			Route::get('products', 'ProductsController@list');
			Route::post('products', 'ProductsController@store');
			Route::put('products/{id}', 'ProductsController@update')->where(['id'=>$regex_id_int]);
			Route::delete('products/{id}', 'ProductsController@destroy')->where(['id'=>$regex_id_int]);
			/**/
			Route::get('gps_devices', 'GpsDeviceController@list');
			Route::post('gps_devices', 'GpsDeviceController@store');
			Route::put('gps_devices/{id}', 'GpsDeviceController@update')->where(['id'=>$regex_id_int]);
			Route::delete('gps_devices/{id}', 'GpsDeviceController@destroy')->where(['id'=>$regex_id_int]);
			/**/
			Route::get('clients', 'ClientsController@list');
			Route::post('clients', 'ClientsController@store');
			Route::put('clients/{id}', 'ClientsController@update')->where(['id'=>$regex_id_int]);
			Route::delete('clients/{id}', 'ClientsController@destroy')->where(['id'=>$regex_id_int]);
			/**/
			Route::get('packages', 'PackagesController@list');
			Route::get('packages/delivery_now', 'PackagesController@delivery_now');
			Route::post('packages', 'PackagesController@store');
			Route::put('packages/{id}', 'PackagesController@update')->where(['id'=>$regex_id_int]);
			Route::delete('packages/{id}', 'PackagesController@destroy')->where(['id'=>$regex_id_int]);
			Route::get('packages/products/{id}', 'PackagesController@get_products')->where(['id'=>$regex_id_int]);

			Route::post('gpstracking', 'GpsTrackerController@list');
		}); 
	
	});
}); 


	Route::prefix('android')->group(function () {  
		Route::get('gpstracking/{date?}/{imei?}', 'GpsTrackerController@list');
	});
