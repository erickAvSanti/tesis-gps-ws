var fs = require('fs');
var net = require('net');
var mm = require('moment-timezone');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

var dbo = undefined;

MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  dbo = db.db("gps-tesis");
  
});


net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  //var now = new Date(mm.tz("America/Lima").format("MM DD, YYYY hh:mm:ss"));
  var now = new Date();

  // Handle incoming messages from clients.
  socket.on('data', function (raw) {
    if(typeof raw == "object"){
      raw = raw.toString();
      raw = raw.replace(/[^\w\+\-\{\}\:\,\"\'\.]+/g,"");

    } 
    console.log ('Incoming data: '+ raw); 
	/*
	\( start
	1 ([\d]{12}) imei
	2 ([\w]+) command
	3 ([\d]{6}) date
	4 ([AV]) validate | invalidate
	5 ([\d]{2}) latitude degree
	6 ([\d]{2}\.[\d]{4}) latitude minutes
	7 ([NS]) north | south
	8 ([\d]{3}) longitude degree
	9 ([\d]{2}\.[\d]{4}) longitude minutes
	10 ([EW]) longitude east | west
	11 ([\d]{3}\.[\d]) speed
	12 ([\d]{6}) time
	13 ([\d]{3}\.[\d]{2}) angle
	14 ([\d]{8}L[\d]{8}) io state | L | milea data

	*/
	
	/*
	{
		"timestamp":"2018-08-26T01:45:00.225Z",
		"lat":"NaN",
		"long":"NaN",
		"trackerId":"087075577324",
		"trackerDate":"BR00",
		"speed":null,
		"origData":"(087075577324BR00180825V1212.8802S07656.4291W006.4211407000.00,00000000L00000000)"}
	parsed array:(
		087075577324BR00180825V1212.8802S07656.4291W006.4211407000.00,00000000L00000000),
		087075577324,
		BR00,
		180825,
		V,
		12,
		12.8802,
		S,
		076,
		56.4291,
		W,
		006.4,
		211407,
		000.00,
		00000000L00000000
	(087075577324BR00180825V1212.8802S07656.4291W006.4211407000.00,00000000L00000000)
	*/
	//3.2.6 Isochronous for continues feedback messagE
	var re_isochronous_for_cont_feedback_msg = /([\d]{12})([\w]+)([\d]{6})([AV])([\d]{2})([\d]{2}\.[\d]{4})([NS])([\d]{3})([\d]{2}\.[\d]{4})([EW])([\d]{3}\.[\d])([\d]{6})([\d]{3}\.[\d]{2}),([\d]{8}L[\d]{8})/;
	//3.2.1 Handshake signal Messag
	var re_handshake_signal_message = /([\d]{12})([\w]+)HSO/;
    var m;
    var doc;
    if ((m = re_isochronous_for_cont_feedback_msg.exec(raw)) !== null) {
        if (m.index === re_isochronous_for_cont_feedback_msg.lastIndex) {
            re_isochronous_for_cont_feedback_msg.lastIndex++;
        }
    	var lat = (parseInt(m[5])+parseFloat(m[6])/60.00).toFixed(5);
    	var long = (parseInt(m[8])+parseFloat(m[9])/60.00).toFixed(5);
        if(m[7]=="S"){
        	lat = -lat;
        }
        if(m[10]=="W"){
        	long = -long;
        }
        doc = {
          timestamp: now,
          imei:m[1],
        	command: m[2],
        	date: m[3],
        	time: m[12],
        	angle: m[13],
        	gps_status: m[4],
        	lat_orientation: m[7],
        	long_orientation: m[10],
        	lat:lat,
        	ln:long, 
        	speed: parseFloat(m[11]),
        	io_state_milea_data: m[14],
          origData: m[0], 
        }; 
    }else if ((m = re_handshake_signal_message.exec(raw)) !== null) {
    	if (m.index === re_isochronous_for_cont_feedback_msg.lastIndex) {
            re_isochronous_for_cont_feedback_msg.lastIndex++;
        }
        /*
        doc = {
            timestamp: new Date(),
            imei:m[1], 
        };  
        */
    }else{
      try{
        var json = JSON.parse(raw);
        doc = {
          lat:json.lat,
          ln:json.ln,
          imei:json.imei,
          phone_number:json.phone_number,
          phone_sim_operator_name:json.phone_sim_operator_name,
          phone_sim_serial_number:json.phone_sim_serial_number,
          timestamp:now,
        };
      }catch(e){
        console.log(e.stack);
      }
    }
    if(doc){
  		console.log("inserting... ",doc);
  		if(dbo){
  			dbo.collection("tracking").insertOne(doc, function(err, res) {
  			    if (err) throw err;
  			});
  		}else{
  			console.log("dbo undefined");
  		}
    }
	socket.emit("end");
  });

  socket.on('error', function (err) {
  	console.log(err);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
  	//console.log(socket.name+" end, "+(new Date()).toLocaleString('es-PE'));
  }); 
}).listen(8284);
 